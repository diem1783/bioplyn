import { BioplynPage } from './app.po';

describe('bioplyn App', () => {
  let page: BioplynPage;

  beforeEach(() => {
    page = new BioplynPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
